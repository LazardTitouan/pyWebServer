import fileLoader
import mimetypes
import os
import environment
import paramDic


class Request:

	#cette objet permet de traiter les requete des clients
	
	def __init__(self,server,data,port):
		#tout d'abor on recupere les parametre http de la requete du client
		self.dict = {"METHODE":None,"":None,"Host":None,"Referer":None,"User-Agent":None,"Connection":None,"Accept":None,"Accept-Charset":None,"Accept-Language":None}
		self.param = paramDic.ParamDic()
		self.requestedRes = ""
		self.cookies = {}
		self.server = server
		tmpDat = data.split("\n")
		self.dict["METHODE"]=tmpDat[0].split(" ")[0]
		for line in tmpDat[1:]:
			line = line.split(":",1)
			if(line[0] in self.dict.keys() and len(line[0])>3):
				self.dict[line[0]] = line[1]
			elif (line[0]=="Cookie"):
				self.getCookies(line);
		self.extractParam(data)
		self.requestedRes = "base/" + tmpDat[0].split(" ")[1].split("?")[0]
		self.determinePage()

	

	#c'est la fonction appeller pour envoyer la reponse
	def execute(self):
		#on charge le fichier demander
		content = fileLoader.loadFile(self)
		mime = mimetypes.guess_type(self.requestedRes)
		if(mime==(None,None)):
			mime=("text/text",None)
		length = len(content)

		toSend = ("HTTP/1.0 "+self.statu+"\n Content-Type: "+mime[0]+" \n"+ self.addedCookies() +"Content-Length: " + str(length)+ " \n\n").encode()+content
		return toSend


	#determine quel page doit etre envoyer (celle demander, 404,403,list)
	def determinePage(self):
		self.statu="200 OK"
		if(fileLoader.getFirstDir(os.path.normpath(os.path.dirname(self.requestedRes))) not in environment.allowedPath) or (os.path.dirname(os.path.normpath(self.requestedRes)) in environment.unAllowedPath):
			self.statu = "403 FORBIDEN"
			self.requestedRes="static/403.html"
		elif(self.requestedRes[-1]=="/" and os.path.exists(os.path.normpath(self.requestedRes))):
			if(os.path.isfile(self.requestedRes+"index.html")):
				self.requestedRes+="index.html"
			else:
				self.param.add("requestedRes",os.path.normpath(self.requestedRes))
				self.requestedRes="static/list.html"	
		elif(not os.path.isfile(self.requestedRes)):
			self.requestedRes="static/404.html"
			self.statu = "404"

		self.requestedRes= os.path.normpath(self.requestedRes)


	#extrait les parametre passer en get ou en post et les stock dans un dico
	def extractParam(self,data):
		if(self.dict["METHODE"]=="GET"):
			isKey=True
			begin=False
			key=""
			value=""
			i=0
			while data[i]!="\n" and not (begin and data[i]==" "):
				lettre=data[i]
				if lettre=="?":
					begin=True
				elif begin:
					if lettre=="&":
						isKey=True
						self.param.add(key,value)
						key=""
						value=""
					elif lettre=="=":
						isKey=False
					else:
						if isKey:
							key+=lettre
						else:
							value+=lettre
				i+=1
			self.param.add(key,value)
		elif(self.dict["METHODE"]=="POST"):
			isKey=True
			key=""
			value=""
			for lettre in data.split("\n")[-1]:
				if lettre=="&":
					isKey=True
					self.param.add(key,value)
					key=""
					value=""
				elif lettre=="=":
					isKey=False
				else:
					if isKey:
						key+=lettre
					else:
						value+=lettre
			self.param.add(key,value)


	
	def createCookie(self,name,value):
		self.cookies[name]=value


	def addedCookies(self):
		if len(self.cookies)!=0:
			return "Set-Cookie:"+"".join([elem+"="+self.cookies[elem]+";" for elem in self.cookies])+"\n"
		else:
			return""

	def getCookies(self,line):
		elem = line[1][1:].split(";")
		for cookie in elem:
			cookie =cookie.split("=")
			if (cookie[1][-1]=="\r"):
				cookie[1]=cookie[1][:-1]
			self.param.add(cookie[0],cookie[1])
	