import socket
import threading
import request
import sys
import select 
#c'est le point d'entré du programme

class Server:
	def __init__(self,port):
		sock = socket.socket()
		sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)
		sock.bind(("0.0.0.0",port))
		sock.listen(10)
		while True:
			demande,x,w = select.select([sock],[], [], 0.05)
			accepte = []
    		
			for connexion in demande:
				print("demmande")
				print(connexion)
				connexion_avec_client, infos_connexion = connexion.accept()
				accepte.append(connexion_avec_client)
			
			try:
				requetes,_,_=select.select(accepte,[],[],0.05)
			except:
				pass
			else:
				for requete in requetes:
					data = requete.recv(1024).decode()
					if len(data)!=0:
			#on crée un objet Request
						req=request.Request(self,data,port)
						requete.send(req.execute())
				for connexion in accepte:
					connexion.close()


if __name__=="__main__":
	port=80
	if(len(sys.argv)>1):
		try:
			port = int(sys.argv[1])
		except:
			print("Impossible de lancer le server sur le port")
	s = Server(port);