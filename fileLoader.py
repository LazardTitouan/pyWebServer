import os
import environment
from page import *
import types

def loadFile(request):
	content = ""
	try:
		m = open(request.requestedRes,"rb")
		content = m.read()
	except: 
		content = b"Erreur de chargement des fichier"
		print("Erreur de chargement du fichier " +request.requestedRes)
	#si le fichier est un fichier html et qu'il commence par le caractere @ c'est que le fichier est un template
	if((request.requestedRes[-4:]=="html") and (content[0]==64)):
		res,content = content.decode("utf-8").split("\n",1)
		content = parse(content,res[1:],request)# derriere le symbole "@" se trouve le nom du fichier python a joindre au template (defini dans environment.py)
	return content


def parse(content,res,request):
	# try:
	# 	obj = eval(res+"(content,request)")
	# 	return obj.execute(request.param).encode()#on recupere le fichier a joindre et on appel la méthode execute cett méthode remplit le template
	# except:
	# 	print("Error no such resource to join")
	# 	return "INTERNAL ERROR".encode()
	obj = eval(res+"(content,request)")
	return obj.execute(request.param).encode()


def getFirstDir(requestedRes):
	res = ""
	i=0
	while (i<len(requestedRes) and requestedRes[i]!="/"):
		res+=requestedRes[i]
		i+=1
	return res
	
