from page.joinable import *
from page.environmentChat import listUser

class MainPage(Joinable):


	def __init__(self,content,request):
		Joinable.__init__(self,content,request)

	def execute(self,params):
		if (params.isSet("cookieSession")):
			if(params.get("cookieSession") in listUser):
					self.plant("content",self.include("toBeInclude/chat.html"))
			else:
				self.plant("content",self.include("toBeInclude/login.html"))

		else:
			self.plant("content",self.include("toBeInclude/login.html"))
		return self.getFinalText()

