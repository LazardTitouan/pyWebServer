from .user import *
import page.environmentChat 

class AdminUser(User):
	def __init__ (self,name,color):
		self.name = "Admin"
		self.color = "red"
		page.environmentChat.listMessage.append("style=red> * Un admin Arrive!!")

	def send(self,params):
		if(params.get("mess")[0]=="/"):
			action,args = params.get("mess").split(" ")
			action = action[1:]
			action ="commande_" +action
			try:
				getattr(self,action)(args)
			except:
				print("Error: no such method : " + action)
		else:
			User.send(self,params)


	def commande_kick(args):
		print(args)
