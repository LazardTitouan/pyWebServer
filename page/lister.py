from page.joinable import *
import os

class Lister(Joinable):
	
	def __init__(self,content,request):
		Joinable.__init__(self,content,request)

	def execute(self,params):
		self.plant("requestedRes", params.get("requestedRes"))
		self.plant("liste", Lister.makeList(params.get("requestedRes")))
		return self.getFinalText()

	def makeList(dire,base = "./"):
		res = "<ul> "
		try:
			for x in os.listdir(dire):
				if os.path.isdir(dire+"/"+x):
					res+="<li><a href="+base+x+"/>"+x+"/</a></li>"
				else:
					res+="<li><a href="+base+x+">"+x+"</a></li>"
			res+= "</ul>"
		except:
			res =  "Aucun dossier selectionner"
		return res