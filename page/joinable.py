import fileLoader

class Joinable:

	# c'est la super class dont tout les fichier .py qui sont join a des fichier html doivent heriter.
	def __init__(self,content,request):
		#on recupere les point d'entré dans le template
		self.entryPoints={}
		self.content=content
		self.request=request
		self.cookies = {}
		

	def plant(self,key,value):
		#au point d'entré nommé "key" on ferat la modif "value"
		self.entryPoints[key]=value
		

	
	def getFinalText(self):
		
		res = ""
		
		i=0
		while i<len(self.content)-1:
			if(self.content[i]=="{" and self.content[i-1]!="!"):
				name = ""
				entryCoor = i
				i+=1
				while i<len(self.content) and self.content[i]!="}" :
					name+=self.content[i]
					i+=1
				if(name in self.entryPoints.keys()):
					res+=self.entryPoints[name]
			else:
				if(not (self.content[i]=="!" and self.content[i+1]=="{")):
					res += self.content[i]
			i+=1
		return res


	def include(self,f):
		oldFile = self.request.requestedRes
		self.request.requestedRes = f
		res = fileLoader.loadFile(self.request)
		self.request.requestedRes = oldFile
		return res.decode("utf-8")

