import page.environmentChat
import urllib
import paramDic

class User:

	def __init__(self,name,color):
		self.name = paramDic.sanitizeInput(name,paramDic.xss)
		self.color = color	

	def send(self,params):
			mess =  params.sanitizeInput(urllib.parse.unquote(urllib.parse.unquote(params.get("mess"))),paramDic.xss)
			mess = "style='color:"+page.environmentChat.listUser[params.get("cookieSession")].color+"'>"+page.environmentChat.listUser[params.get("cookieSession")].name + ": " + mess
			page.environmentChat.listMessage.append(mess)
			if(len(page.environmentChat.listMessage)>page.environmentChat.tailleMax):
				page.environmentChat.listMessage.remove(page.environmentChat.listMessage[0])