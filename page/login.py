from page.joinable import *
from page.environmentChat import *
from page.user import *
import random
import urllib

class Login(Joinable):
	
	def __init__(self,content,request):
		Joinable.__init__(self,content,request)


	def execute(self,params):
		t = False
		us = None 
		if(params.isSet("name")):
			for elem in  listUser :
				if(listUser[elem].name == params.get("name")):
					self.plant("erreur","<p>Login Already in use</p>")
					t=True
					break
			if(not t and (params.get("name")not in [x[0] for x in specialUser])):
				us = User(urllib.parse.unquote(urllib.parse.unquote(params.get("name"))),params.get("color"))
			if((params.get("name"),params.get("pass")) in specialUser):
				us = specialUser[(params.get("name"),params.get("pass"))](urllib.parse.unquote(urllib.parse.unquote(params.get("name"))),urllib.parse.unquote(params.get("color")))		
			if (us != None):
				self.plant("redirect","<meta http-equiv=\"refresh\" content=0; URL=/\">")
				cook = "".join([chr(random.randint(97,122)) for _ in range(30)])
				self.request.createCookie("cookieSession",cook)
				listUser[cook] = us
		return self.getFinalText()