import re

lfi = ["\.","/"]
sqlNum = ["[^0-9]"]
sqlText = ["'",'"']
xss = ["<",">",""]

class ParamDic:
	def __init__(self):
		self.dic= {}


	def add(self,key,value):
		self.dic[key]=value

	def get(self,key,safemode=[]):
		try:
			return self.sanitizeInput(self.dic[key],safemode)
		except:
			return ""

	def isSet(self,key):
		return (key in self.dic.keys())

	def sanitizeInput(self,value, rem):
		for elem in rem :
			m = re.compile(elem)
			value = m.sub("",value)
		return value
		

